package Model.Mathang;

import java.io.Serializable;

public class Mathang  implements Serializable {

	private int id;
	private String ten;
	private float gia;
	private int soluong;
	private String mota;
	private String cannang;
	
	public Mathang() {
		
	}
	public Mathang(String ten, String mota, float gia) {
		super();
		this.ten = ten;
		this.mota = mota;
		this.gia = gia;
	}
	
	
	
	public Mathang(int id, String ten, float gia, int soluong, String mota, String cannang) {
		super();
		this.id = id;
		this.ten = ten;
		this.gia = gia;
		this.soluong = soluong;
		this.mota = mota;
		this.cannang = cannang;
	}
//	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public float getGia() {
		return gia;
	}
	public void setGia(float gia) {
		this.gia = gia;
	}
	public int getSoluong() {
		return soluong;
	}
	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}
	public String getMota() {
		return mota;
	}
	public void setMota(String mota) {
		this.mota = mota;
	}
	public String getCannang() {
		return cannang;
	}
	public void setCannang(String cannang) {
		this.cannang = cannang;
	}

	
}