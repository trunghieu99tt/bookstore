package Controller.MuahangDAO;

import Model.Muahang.*;

public interface ThanhtoanDAO {

	/**
	 * 
	 * @param t
	 */
	void create(Thanhtoan t);

	/**
	 * 
	 * @param g
	 */
	void getCart(Giohang g);

	/**
	 * 
	 * @param p
	 */
	void getSale(Phieugiamgia p);

	/**
	 * 
	 * @param p
	 */
	void getShip(Phuongthucvanchuyen p);


}