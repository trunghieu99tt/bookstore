package Controller.MuahangDAO;

import Model.Muahang.*;
import Model.Khachhang.*;

public interface DonhangDAO {

	/**
	 * 
	 * @param d
	 */
	void create(Donhang d);

	/**
	 * 
	 * @param d
	 */
	void update(Donhang d);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 
	 * @param k
	 */
	void getKH(Khachhang k);

	/**
	 * 
	 * @param p
	 */
	void getShip(Phuongthucvanchuyen p);

	/**
	 * 
	 * @param t
	 */
	void getPay(Thanhtoan t);

}