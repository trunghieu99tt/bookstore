package Controller.MuahangDAO;

import Model.Muahang.*;
import Model.Mathang.*;

public interface HangTrongGioDAO {

	/**
	 * 
	 * @param h
	 */
	void create(HangTrongGio h);

	/**
	 * 
	 * @param g
	 */
	void getCart(Giohang g);

	/**
	 * 
	 * @param m
	 */
	void getItem(Mathang m);


}