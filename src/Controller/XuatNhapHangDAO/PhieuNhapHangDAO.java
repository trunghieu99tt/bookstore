package Controller.XuatNhapHangDAO;

import Model.XuatNhapKho.*;
import Model.Nhanvien.*;
import Model.Mathang.*;

public interface PhieuNhapHangDAO {

	/**
	 * 
	 * @param p
	 */
	void create(PhieuNhapHang p);

	/**
	 * 
	 * @param n
	 */
	void getNVQLK(NVQuanlyKho n);

	/**
	 * 
	 * @param s
	 */
	void getSach(Sach s);

	/**
	 * 
	 * @param v
	 */
	void getVPP(Vanphongpham v);

	/**
	 * 
	 * @param n
	 */
	void getNSX(Nhasanxuat n);

	/**
	 * 
	 * @param n
	 */
	void getNXB(Nhaxuatban n);

}