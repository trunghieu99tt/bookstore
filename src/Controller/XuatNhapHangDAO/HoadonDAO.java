package Controller.XuatNhapHangDAO;

import Model.XuatNhapKho.*;
import Model.Nhanvien.*;

public interface HoadonDAO {

	/**
	 * 
	 * @param h
	 */
	void create(Hoadon h);

	/**
	 * 
	 * @param n
	 */
	void getNVBH(NVBanhang n);

}