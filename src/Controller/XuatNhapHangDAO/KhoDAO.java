package Controller.XuatNhapHangDAO;

import Model.XuatNhapKho.*;

public interface KhoDAO {

	/**
	 * 
	 * @param k
	 */
	void create(Kho k);

	/**
	 * 
	 * @param k
	 */
	void update(Kho k);

}