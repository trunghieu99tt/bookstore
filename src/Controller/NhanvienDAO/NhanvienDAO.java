package Controller.NhanvienDAO;

import Model.Nhanvien.*;

public interface NhanvienDAO {

	/**
	 * 
	 * @param n
	 */
	void create(Nhanvien n);

	/**
	 * 
	 * @param n
	 */
	void update(Nhanvien n);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 
	 * @param g
	 */
	void getTime(Giolam g);

}