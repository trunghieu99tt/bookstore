package Controller.NhanvienDAO;

import Model.Nhanvien.*;

public interface GiolamDAO {

	/**
	 * 
	 * @param g
	 */
	void create(Giolam g);

	/**
	 * 
	 * @param g
	 */
	void update(Giolam g);

	/**
	 * 
	 * @param n
	 */
	void getNV(Nhanvien n);

	/**
	 * 
	 * @param n
	 */
	void getNgay(Ngaylam n);

}