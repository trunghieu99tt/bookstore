package Controller.Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Controller.MuahangDAO.*;
import Model.Mathang.Mathang;
import Model.Muahang.Giohang;


@WebServlet("/giohang")
public class GiohangServlet extends HttpServlet {

	private GiohangDAO giohangDao = new GiohangDAOImpl();

	public void doPost() {
		// TODO - implement GiohangServlet.doPost
		throw new UnsupportedOperationException();
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)   throws ServletException, IOException{
		// TODO - implement GiohangServlet.doGet
		HttpSession session = req.getSession();
		Giohang gioHang = (Giohang) session.getAttribute("giohang");
		if (gioHang == null)
		{
			gioHang = new Giohang();
			session.setAttribute("giohang", gioHang);
		}
		
		ArrayList<Mathang> hangTrongGio = giohangDao.gethangtronggio();
		
		
		session.setAttribute("giohang", gioHang);
		
		req.setAttribute("listMatHang", hangTrongGio);
		
		RequestDispatcher dispatcher =
	             getServletContext().getRequestDispatcher("/showgiohang.jsp");
	        dispatcher.forward(req, res);      
		return ;
	}

	public void update() {
		// TODO - implement GiohangServlet.update
		throw new UnsupportedOperationException();
	}

}