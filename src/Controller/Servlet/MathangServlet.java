package Controller.Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Controller.MathangDAO.MathangDAOImpl;
import Model.Mathang.Mathang;


@WebServlet("/mathang")
public class MathangServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private MathangDAOImpl mathangDAOImpl;
	
	public void init() {
//      String jdbcURL = getServletContext().getInitParameter("jdbcURL");
//      String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
//      String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
//
//      bookDAO = new BookDAO(jdbcURL, jdbcUsername, jdbcPassword);
  	  	mathangDAOImpl = new MathangDAOImpl();
	}
	
	public void create(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		// TODO - implement MathangServlet.create
		
		String ten = request.getParameter("title");
        String mota = request.getParameter("author");
        float gia = Float.parseFloat(request.getParameter("price"));
 
        // Mathang newMathang = new Mathang(ten, mota, gia);
        // Chua xong	
//        mathangDAOImpl.create(newMathang);
        // response.sendRedirect("list");
        return ;
//		throw new UnsupportedOperationException();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
      doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
		String action = request.getServletPath();
		ArrayList<Mathang> listMatHang = this.mathangDAOImpl.getAll();
		for (Mathang matHang: listMatHang) {
			System.out.println(matHang.toString());
		}
		HttpSession session = request.getSession();
		session.setAttribute("listMatHang", listMatHang);
		
		RequestDispatcher dispatcher =
	             getServletContext().getRequestDispatcher("/ItemList.jsp");
	        dispatcher.forward(request, response);      

		
		return ;
	  }
//	public void doPost(HttpServletRequest request, HttpServletResponse response)
//    throws ServletException, IOException {
//		// TODO - implement MathangServlet.doPost
//		throw new UnsupportedOperationException();
//		doGet(request, response);
//	}
//
//	public void doGet(){
//		throw new UnsupportedOperationException();
//}

	public void update() {
		// TODO - implement MathangServlet.update
		throw new UnsupportedOperationException();
	}

	public void delete() {
		// TODO - implement MathangServlet.delete
		throw new UnsupportedOperationException();
	}

	public void getSach() {
		// TODO - implement MathangServlet.getSach
		throw new UnsupportedOperationException();
	}

	public void getVPP() {
		// TODO - implement MathangServlet.getVPP
		throw new UnsupportedOperationException();
	}

	public void search() {
		// TODO - implement MathangServlet.search
		throw new UnsupportedOperationException();
	}

	public void getComment() {
		// TODO - implement MathangServlet.getComment
		throw new UnsupportedOperationException();
	}
	
}