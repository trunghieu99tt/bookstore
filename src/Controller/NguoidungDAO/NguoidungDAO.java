package Controller.NguoidungDAO;

import Model.Nguoidung.*;

public interface NguoidungDAO {

	/**
	 * 
	 * @param n
	 */
	void create(Nguoidung n);

	/**
	 * 
	 * @param n
	 */
	void update(Nguoidung n);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

}