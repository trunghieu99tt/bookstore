package Controller.KhachhangDAO;

import Model.Khachhang.*;
import Model.Muahang.*;

public interface KhachhangDAO {

	/**
	 * 
	 * @param k
	 */
	void create(Khachhang k);

	/**
	 * 
	 * @param k
	 */
	void update(Khachhang k);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 
	 * @param d
	 */
	void getOrder(Donhang d);

	/**
	 * 
	 * @param l
	 */
	void getHistory(LichsuXemHang l);

}