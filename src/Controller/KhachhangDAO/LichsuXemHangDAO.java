package Controller.KhachhangDAO;

import Model.Khachhang.*;

public interface LichsuXemHangDAO {

	/**
	 * 
	 * @param l
	 */
	void create(LichsuXemHang l);

	/**
	 * 
	 * @param l
	 */
	void update(LichsuXemHang l);

}