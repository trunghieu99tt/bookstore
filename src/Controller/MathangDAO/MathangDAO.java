package Controller.MathangDAO;

import java.sql.SQLException;

import Model.Mathang.*;

public interface MathangDAO {

	/**
	 * 
	 * @param m
	 */
	boolean create(Mathang m) throws SQLException ;

	/**
	 * 
	 * @param m
	 */
	void update(Mathang m);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 
	 * @param s
	 */
	void getSach(Sach s);

	/**
	 * 
	 * @param v
	 */
	void getVPP(Vanphongpham v);

	/**
	 * 
	 * @param key
	 */
	void search(String key);

	/**
	 * 
	 * @param b
	 */
	void getComment(Binhluan b);

}