package Controller.MathangDAO;

import Model.Mathang.*;
import Model.Khachhang.*;

public interface BinhluanDAO {

	/**
	 * 
	 * @param b
	 */
	void create(Binhluan b);

	/**
	 * 
	 * @param id
	 */
	void delete(int id);

	/**
	 * 
	 * @param k
	 */
	void getKH(Khachhang k);

}