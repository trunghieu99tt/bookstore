package Controller.MathangDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.net.ssl.CertPathTrustManagerParameters;

import Model.Mathang.*;

public class MathangDAOImpl implements MathangDAO {
	
	private String jdbcURL = "jdbc:mysql://localhost:3306/bookstore?useSSL=false";
    private String jdbcUsername = "root";
    private String jdbcPassword = "maybietlamgi";
    private Connection jdbcConnection;
    
    public void MathangDAO() {
    	
    }
    
    public void MathangDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }
     
    protected void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(
                                        jdbcURL, jdbcUsername, jdbcPassword);
        }
    }
     
    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }
	/**
	 * 
	 * @param m
	 * @return 
	 * @throws SQLException 
	 */
    
    
	public boolean create(Mathang m) throws SQLException {
		String sql = "INSERT INTO book (title, author, price) VALUES (?, ?, ?)";
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, m.getTen());
        statement.setString(2, m.getMota());
        statement.setFloat(3, m.getGia());
         
        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
	}

	public ArrayList<Mathang> getAll()  {
		String  sql = "SELECT * FROM bookstore.Mathang;";
		ArrayList<Mathang> listMatHang = new ArrayList<Mathang>(); 
		try {

			connect();
			
			Statement stmt = (Statement) jdbcConnection.createStatement();
			ResultSet result = (ResultSet) stmt.executeQuery(sql);
			while (result.next()) {
				int id = Integer.parseInt(result.getString("id"));
				String ten = result.getString("ten");
				float gia = Float.parseFloat(result.getString("gia"));
				int soluong = Integer.parseInt(result.getString("soluong"));
				String mota = result.getString("mota");
				String cannang = result.getString("cannang");
				Mathang hang = new Mathang(id, ten, gia, soluong, mota, cannang);
				listMatHang.add(hang);
			}
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMatHang;
	}
	
	/**
	 * 
	 * @param m
	 */
	public void update(Mathang m) {

	}

	/**
	 * 
	 * @param id
	 */
	public void delete(int id) {
		// TODO - implement MathangDAOImpl.delete
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param s
	 */
	public void getSach(Sach s) {

	}

	/**
	 * 
	 * @param v
	 */
	public void getVPP(Vanphongpham v) {

	}

	/**
	 * 
	 * @param key
	 */
	public void search(String key) {
		// TODO - implement MathangDAOImpl.search
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param b
	 */
	public void getComment(Binhluan b) {

	}

}