package Controller.MathangDAO;

import Model.Mathang.*;

public interface SachDAO {

	/**
	 * 
	 * @param s
	 */
	void create(Sach s);

	/**
	 * 
	 * @param s
	 */
	void update(Sach s);

	/**
	 * 
	 * @param t
	 */
	void getType(Theloai t);

}