<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Books Store Application</title>
</head>
<body>
    <center>
        <h1>Item Management</h1>
        <h2>
            <a href="new">Add New Item</a>
            &nbsp;&nbsp;&nbsp;
            <a href="list">List All Item</a>
            
            
            
            
             
        </h2>
    </center>
    <div align="center">
        <c:if test="${mathang != null}">
            <form action="update" method="post">
        </c:if>
        <c:if test="${mathang == null}">
            <form action="insert" method="post">
        </c:if>
        <table border="1" cellpadding="5">
            <caption>
                <h2>
                    <c:if test="${mathang != null}">
                        Edit Item
                    </c:if>
                    <c:if test="${mathang == null}">
                        Add New Item
                    </c:if>
                </h2>
            </caption>
                <c:if test="${mathang != null}">
                    <input type="hidden" name="id" value="<c:out value='${mathang.id}' />" />
                </c:if>           
            <tr>
                <th>Title: </th>
                <td>
                    <input type="text" name="title" size="45"
                            value="<c:out value='${mathang.ten}' />"
                        />
                </td>
            </tr>
            
            <tr>
                <th>Price: </th>
                <td>
                    <input type="text" name="price" size="5"
                            value="<c:out value='${mathang.gia}' />"
                    />
                </td>
            </tr>
            
            <tr>
                <th>Amount: </th>
                <td>
                    <input type="text" name="price" size="5"
                            value="<c:out value='${mathang.soluong}' />"
                    />
                </td>
            </tr>
            
            <tr>
                <th>Description: </th>
                <td>
                    <input type="text" name="price" size="5"
                            value="<c:out value='${mathang.mota}' />"
                    />
                </td>
            </tr>
            
            <tr>
                <th>Weight: </th>
                <td>
                    <input type="text" name="author" size="45"
                            value="<c:out value='${mathang.cannang}' />"
                    />
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Save" />
                </td>
            </tr>
        </table>
        </form>
    </div>   
</body>
</html>