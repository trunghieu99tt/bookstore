<%@page import="java.util.ArrayList"%>
<%@page import="Model.Mathang.Mathang"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Books Store Application</title>
</head>
<body>

	

    <center>
        <h1>Giỏ hàng</h1>
        <h2>
            <a href="new">Hiển thị hàng trong giỏ</a>
            &nbsp;&nbsp;&nbsp;
            <a href="list">List All Books</a>
             
        </h2>
    </center>
    <div align="center">
    	
        <table border="1" cellpadding="5">
            <caption><h2>List of Books</h2></caption>
             <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Giá</th>
                <th>Số lượng</th>
                <th>Mô tả</th>
            <c:forEach var="mathang" items="${listMatHang}">
                <tr>
                    <td><c:out value="${mathang.id}" /></td>
                    <td><c:out value="${mathang.ten}" /></td>
                    <td><c:out value="${mathang.gia}" /></td>
                    <td><c:out value="${mathang.soluong}" /></td>
                    <td><c:out value="${mathang.mota}" /></td>
                    <td>
                        <a href="edit?id=<c:out value='${mathang.id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="delete?id=<c:out value='${mathang.id}' />">Delete</a>                     
                    </td>
                </tr>
            </c:forEach>
                   </table>
    </div>   
</body>
</html>>